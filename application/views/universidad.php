<br>

<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/logo'); ?>" alt="">
<br>
<div id="mapa1" style="width:100%; height:300px; border:2px solid black">


</div>
<script type="text/javascript">
function initMap(){
  //creando una coordenada
  var coordenadaCentral= new google.maps.LatLng(-0.9177800683577261, -78.63296434263123);
  var mimapa=new google.maps.Map(
    document.getElementById('mapa1'),
    {
      center:coordenadaCentral,
      zoom:9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
  );

  var marcadorUTC= new google.maps.Marker({
    position:new google.maps.LatLng(-0.9152280504593906, -78.63303078561164),
    map: mimapa,
    title: 'UTC MATRIZ'
    

  });
  var marcadorSalache= new google.maps.Marker({
    position:new google.maps.LatLng(-0.9993576952905089, -78.62337123611675),
    map: mimapa,
    title: 'UTC Campus Salache'

  });
  var marcadorPujili= new google.maps.Marker({
    position:new google.maps.LatLng(-0.958101823584964, -78.6964806293675),
    map: mimapa,
    title: 'UTC Extencion Pujili'

  });

var marcadorLaMana= new google.maps.Marker({
  position:new google.maps.LatLng(-0.9301107004391356, -79.2363974983071),
  map: mimapa,
  title: 'UTC Campus La mana'

});

}
</script>
