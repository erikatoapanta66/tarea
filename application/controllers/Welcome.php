<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->view('header'); //Cargando Cabecera
		$this->load->view('welcome_message'); //Cargando Cuerpo
		$this->load->view('footer'); //Cargando PIE
	}

	public function universidad()
	{

		$this->load->view('header'); //Cargando Cabecera
		$this->load->view('universidad'); //Cargando Cuerpo
		$this->load->view('footer'); //Cargando PIE
	}
	public function ciudad()
	{

		$this->load->view('header'); //Cargando Cabecera
		$this->load->view('ciudad'); //Cargando Cuerpo
		$this->load->view('footer'); //Cargando PIE
	}
}// Cierre de Clase
